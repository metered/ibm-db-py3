test_dir	=	'tests'		# Location of testsuite file (relative to current directory) (Don't change this.)

database	=	'TEST'	# Database to connect to. Please use an empty database for best results.
user		=	'db2'	# User ID to connect with
password	=	'22'	# Password for given User ID
hostname	=	'localhost'	# Hostname
port		=	50000		# Port Number

auth_user	=	'auth_user'	# Authentic user of Database
auth_pass	=	'auth_pass'	# Password for Authentic user
tc_user		=	'tc_user'	# Trusted user
tc_pass		=	'tc_pass'	# Password to trusted user
